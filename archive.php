
<?php
  get_header();
  $seoxCategory = get_queried_object();
?>

<!-- INICIO - HEADER -->
<div id="header">
  <div class="w-100 text-center py-3 bg-seox-black">

    
    <?php 
    
      $custom_logo_id = get_theme_mod('custom_logo');
      $logo = wp_get_attachment_image_src( $custom_logo_id. 'full' );

      if (has_custom_logo( )) {
        echo '<img src="' . esc_url( $logo[0] ) . '" class="img-fluid" alt="Logo Seox">';
      }
    
    ?>

  </div>
  <div class="w-100 bg-secondary p-4 bg-seox-gray-900"></div>

  <div class="container text-white p-2 rounded category bg-seox-gray-800">
    <div class="row gy-sm-2">
      <div class="col-md-6 col-sm-12 d-flex justify-content-center justify-content-sm-center justify-content-md-start align-self-center">
        <h2 class="section text-seox-red-300 text-uppercase"><?= $seoxCategory->name; ?></h2>
      </div>
      <div class="col-md-6 col-sm-12 d-flex justify-content-center justify-content-sm-center justify-content-md-end  align-self-center network">
        <?php get_template_part( 'partials/network' ) ?>
      </div>
    </div>
  </div>
</div>
<!-- FIM - HEADER -->

<!-- INICIO - CONTEUDO -->
<div id="content">
  <div class="container">
    <div class="row mt-4">
      <div class="col-md-8 col-sm-12 mb-sm-5">

      <?php get_template_part( 'partials/post-front' );?>

      </div>
      <div class="col-md-3 offset-md-1 col-sm-12 d-flex flex-column bg-seox-gray-100 p-2">
        <?php get_template_part( 'partials/advertising' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- FIM - CONTEUDO -->
<?php get_footer();?>