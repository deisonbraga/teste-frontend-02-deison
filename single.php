<?php get_header( );?>
    
    <!-- INICIO - HEADER -->
    <div id="header">
      <div class="w-100 bg-black border-top border-dark text-center py-3">

        <a href="<?php bloginfo( 'url' ) ?>">
          <?php 
            
            $custom_logo_id = get_theme_mod('custom_logo');
            $logo = wp_get_attachment_image_src( $custom_logo_id. 'full' );

            if (has_custom_logo( )) {
              echo '<img src="' . esc_url( $logo[0] ) . '" class="img-fluid" alt="Logo Seox">';
            }
          
          ?>
        </a>
      </div>

      <div class="container">

        <?php if(have_posts(  )) : while (have_posts(  )) : the_post(  ); ?>

        <div class="row mt-4 mb-2">
            <h1 class="title-post text-seox-gray-900" ><?php the_title( );?></h1>
            <p class="excerpt-post">
              <!-- Lorem ipsum dolor sit, amet consectetur adipisicing elit. Molestias, possimus dignissimos perferendis sed nam quas? -->
              <?php the_excerpt(  ) ?>
            </p>
        </div>
      </div>

      <div class="container bg-secondary text-white p-3 rounded bg-seox-gray-300">
        <div class="row gy-sm-2">
          <div class="col-md-6 col-sm-12 text-center text-sm-center text-md-start align-self-center  text-seox-gray-900">
            <i class="bi bi-clock"></i><span class="mr-1"> <?php echo get_the_date( 'd/m/y h:m' );?></span>
          </div>
          <div class="col-md-6 col-sm-12 d-flex justify-content-center justify-content-sm-center justify-content-md-end  align-self-center network">
            <?php get_template_part( 'partials/network' ) ?>
          </div>
        </div>
      </div>
    </div>
    <!-- FIM - HEADER -->

    <!-- INICIO - CONTEUDO -->
    <div id="content">
      <div class="container">
        <div class="row mt-4">
          <div class="col-md-8 col-sm-12 mb-sm-5">

            <div class="">

                <!-- <img src="assets/img/Img.jpg" class="img-fluid rounded w-100" alt="" srcset=""> -->
                <?php the_post_thumbnail( 'post-thumbnail', array('class' => 'img-fluid w-100') ) ?>

                <p class="content-post text-seox-gray-900" ><?php the_content( ); ?> </p>

            </div>

            <?php endwhile; ?>
            <?php else : get_404_template(  ); endif ?>

          </div>
          <div class="col-md-3 offset-md-1 col-sm-12 d-flex flex-column">
            <?php get_template_part( 'partials/advertising' ); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- FIM - CONTEUDO -->
<?php get_footer( ) ?>
