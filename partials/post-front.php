<?php if(have_posts(  )) : while (have_posts(  )) : the_post(  ); ?>
    <div class="row mb-5 post">
        <div class="col-md-4 col-sm-12 align-self-center">
        <!-- <img src="assets/img/Img.jpg" class="img-fluid rounded w-100" alt="" srcset=""> -->
        <?php the_post_thumbnail( 'post-thumbnail', array('class' => 'img-fluid w-100') ) ?>
        </div>
        <div class="col-md-8 col-sm-12 align-self-center">
        <div class="content-title">
            <h3 class="feed-2 text-seox-gray-900">
            <a class="link-dark" href="<?php the_permalink( );?>">
                <?php the_title( );?>
            </a>  
            </h3>
        </div>
        <div class="content-date hat">
            <i class="bi bi-clock"></i> <span><?php echo get_the_date( 'd/m/y h:m' );?></span>
        </div>
        </div>
    </div>
<?php endwhile; ?>
<?php else : get_404_template(  ); endif ?>