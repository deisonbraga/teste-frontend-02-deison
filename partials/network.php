<ul>
    <li class="whatsapp">
        <a href=" https://api.whatsapp.com/send?text=<?php the_title( );?>" aria-label="whatsapp"><i class="bi bi-whatsapp"></i> Compartilhar</a>
    </li>
    <li class="telegram">
        <a href="https://telegram.me/share/url?url=<?php the_permalink( );?>&text=<?php the_title( );?>" aria-label="telegram"><i class="bi bi-telegram"></i></a>
    </li>
    <li class="facebook">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink( );?>" aria-label="facebook"><i class="bi bi-facebook"></i></a>
    </li>
    <li class="twitter">
        <a href="https://twitter.com/intent/tweet?url=<?php the_permalink( );?>&text=<?php the_title( );?>" aria-label="twitter"><i class="bi bi-twitter"></i></a>
    </li>
</ul>