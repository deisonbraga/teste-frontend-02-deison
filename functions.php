<?php 

function seox_init_theme() {

    // chamar a tag title
    add_theme_support('title-tag');

    // chamar a logo
    add_theme_support( 'custom-logo' );
}

add_action( 'after_setup_theme', 'seox_init_theme' );


//adicionando o tamanho do resumo
add_filter( 'excerpt_length', function($length) {
    return 20;
});


// Previnindo erros na tag title em versões antigas
if (!function_exists('_wp_render_title_tag')) {
    function seox_render_title () {
        ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php
    }
    add_action('wp_head', 'seox_render_title');
}

// habilitar imagem destacada do post
add_theme_support( 'post-thumbnails' );
set_post_thumbnail( 1230, 720, true );

?>