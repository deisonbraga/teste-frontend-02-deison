<?php get_header( );?>
    
    <!-- INICIO - HEADER -->
    <div id="header">
      <div class="w-100 bg-black border-top border-dark text-center py-3">

        <a href="<?php bloginfo( 'url' ) ?>">
          <?php 
            
            $custom_logo_id = get_theme_mod('custom_logo');
            $logo = wp_get_attachment_image_src( $custom_logo_id. 'full' );

            if (has_custom_logo( )) {
              echo '<img src="' . esc_url( $logo[0] ) . '" class="img-fluid" alt="Logo Seox">';
            }
          
          ?>
        </a>
      </div>

      <div id="error" class="contaienr">

        <div class="row mt-4 mb-2 g-2 d-flex text-center">
            <i class="bi bi-emoji-frown"></i>
            <h2>Ops! Página não encontrada.</h2>
            <div class="col">
            <a class="btn bg-seox-black-20 text-seox-white" href="<?php echo esc_url( home_url( '/' ) );?>">VOLTE PARA HOME</a>
            </div>
        </div>
      </div>
    </div>
    <!-- FIM - CONTEUDO -->
<?php get_footer( ) ?>